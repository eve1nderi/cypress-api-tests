describe('Api automation tests',() => {
    
    it ('GET-list of users',() =>{
        cy.request('https://reqres.in/api/users?page=2').then ((response)=>{
            expect(response.status).equal(200)
            expect(response.body.data).to.have.length(6)
            expect(response.body.data[0].first_name).equal('Michael')
        })
    })

    it ('GET-list of users',() =>{
        cy.request('GET', 'https://reqres.in/api/users?page=2').then ((response)=>{
            expect(response.status).equal(200)
            //expect(resposne.body.data[0].email).equal('michael.lawson@reqres.in')
        })
    })

    it ('POST-Create user',() =>{
        var user={
            "name":"eva",
            "job":"Tester"
        }
        cy.request('POST', 'https://reqres.in/api/users',user).then ((response)=>{
            expect(response.status).equal(201)
            expect(response.body.name).equal(user.name)
            expect(response.body.job).equal(user.job)
        })
    })

    it ('Update-Create user',() =>{
        var user1={
            "name":"eva123",
            "job":"youtuber"
        }
        cy.request('PUT', 'https://reqres.in/api/users/2',user1).then ((response)=>{
            expect(response.status).equal(200)
            expect(response.body.name).equal(user1.name)
            expect(response.body.job).equal(user1.job)
        })
    })
    
    it ('Delete users',() =>{
    cy.request('DELETE', 'https://reqres.in/api/users/2').then ((response)=>{
        expect(response.status).equal(204)
    })
})


})